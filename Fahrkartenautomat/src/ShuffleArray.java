import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
public class ShuffleArray {

	public static void main(String[] args) {
		
	
		//	int[] array = { 1, 2, 3, 4, 5, 6, 7 };
		String input;
		Scanner twister = new Scanner(System.in);
		Random rand = new Random();
		
		
		System.out.println("Hallo User. Dieses Programm twistet ein Wort deiner Wahl.");
		System.out.println("Gebe hier das Wort zum twisten an");
		input = twister.next();
		String s1 = input;
		int length = s1.length();
		System.out.println(length);
		char tempArray[] = input.toCharArray();
	
		for (int i = 1; i < tempArray.length - 1; i++) {
			int randomIndexToSwap = rand.nextInt(tempArray.length -2) +1;
			char temp = tempArray[randomIndexToSwap];
			tempArray[randomIndexToSwap] = tempArray[i];
			tempArray[i] = temp;
		}
		System.out.println(Arrays.toString(tempArray));
	}
}