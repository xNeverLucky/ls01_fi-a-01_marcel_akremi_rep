import java.util.Scanner;

class Fahrkartenautomat
{
	public static double fahrkartenBestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double eingabePreis = 0.0;
		double ticketPreis = 0.0;
		int eingabeAnzahl;
		int ticketCount;
		int ticketTarif;
		//System.out.print("Zu zahlender Betrag (EURO-Cent): ");
	      System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:") ;
	      System.out.println(" Einzelfahrschein Regeltarif AB [2,90 EUR] (1)") ;
	      System.out.println(" Tageskarte Regeltarif AB [8,60 EUR] (2)") ;
	      System.out.println(" Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)") ;
	     ticketTarif = tastatur.nextInt();
	     /* F�r Aufgabe 6.3 nutze ich zuerst einen Boolean der angibt ob die Nutzereingabe g�ltig ist oder nicht. Dann ordne ich mit dem Switch case
	      * den Tarifen ihren entsprechenden Preis zu und �bergebe dann diesen . Die while Schleife dient lediglich dazu,  dass die switch-case-Verzweigung erst endet sobald 
	      * eine g�ltige Eingabe vorliegt (Teil von Aufgabe 5++) */
	    
	     boolean gueltigeEingabe = false;
	    while(gueltigeEingabe == false) {
	     int z = ticketTarif;
	     switch(z) {
	     case 1:
	    	 System.out.println("Ihre Wahl ist 1");
	    	 ticketPreis = 2.90;
	    	 gueltigeEingabe = true;
	    	 break;
	     case 2:
	    	 System.out.println("Ihre Wahl ist 2");
	    	 ticketPreis = 8.60;
	    	 gueltigeEingabe = true;
	    	 break;
	     case 3:
	    	 System.out.println("Ihre Wahl ist 3");
	    	 ticketPreis = 23.50;
	    	 gueltigeEingabe = true;
	    	 break;
	     default:
	    	 gueltigeEingabe = false;
	    	 System.out.println("Ihre Wahl ist " + z);
	    	 System.out.println(">>falsche Eingabe<<");
	    	 ticketTarif = tastatur.nextInt();
	    	 break;
	     }
	     }
	      
	       System.out.print("Anzahl der Tickets : ");
	       
	       eingabeAnzahl = tastatur.nextInt();
	       
	       if(eingabeAnzahl > 0 && eingabeAnzahl <= 10) {
	    	   ticketCount = eingabeAnzahl;
	       }
	       else {
	    	   int i = 0;
	    	   do {
	    		   System.out.println("Ung�ltige Ticketanzahl! Bitte geben sie eine Zahl zwischen 1 und 10 an.");                                       //gleiches Prinzip wie bei dem Preis nur diesmal mit do while Schleife
	    		   i = tastatur.nextInt();
	    		   ticketCount = i;
	    	   }while(i < 0 || i > 10);
	    	    ticketCount = i;
	    	  
	       }
	       double gesamtPreis = ticketCount * ticketPreis;
	       return  gesamtPreis;
	}
	public static double fahrkartenBezahlen(double gesamtPreis) {
		
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneM�nze;   
		Scanner tastatur = new Scanner(System.in); 
		  while(eingezahlterGesamtbetrag < gesamtPreis)
	       {
	    	   double nochZuZahlen = gesamtPreis - eingezahlterGesamtbetrag;
	    	   System.out.printf("Noch zu zahlen: %.2f%n",nochZuZahlen);
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
		  return eingezahlterGesamtbetrag;
	}
	
	public static void fahrscheinAusgabe() {
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");

	}
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double gesamtPreis) {
	       double zwischenWert = eingezahlterGesamtbetrag - gesamtPreis;									//float-point-arithmetik ist f�r bestimmte Recheoperationen aufgrund von Dingen wie Rundungsfehlern ungeeignet daher habe ich einfach
	       double rundungsFehler = zwischenWert * 100;                                                      // den Euro Betrag mit Nachkommazahl in Centbetrag umgerechnet und daf�r dann Integer statt Double benutzt 
	       int rueckgeldAusgeben = (int)rundungsFehler;
	       if(rueckgeldAusgeben > 0.0)
	       {
	    	   System.out.printf("Der R�ckgeldbetrag in H�he von %.2f EURO%n", zwischenWert);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(rueckgeldAusgeben >= 200) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          rueckgeldAusgeben -= 200;
	           }
	           while(rueckgeldAusgeben >= 100) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          rueckgeldAusgeben -= 100;
	           }
	           while(rueckgeldAusgeben >= 50) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgeldAusgeben -= 50;
	           }
	           while(rueckgeldAusgeben >= 20) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgeldAusgeben -= 20;
	           }
	           while(rueckgeldAusgeben >= 10) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgeldAusgeben -= 10;
	           }
	           while(rueckgeldAusgeben >= 5)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgeldAusgeben -= 5;
	           }
	       }
	       		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   			  		"vor Fahrtantritt entwerten zu lassen!\n"+
	       							"Wir w�nschen Ihnen eine gute Fahrt.\n");
	}
		

	
	
	public static void main(String[] args) {
       double bestellungsPreis = 0.0;
       double eingezahlterGesamtbetrag = 0.0;
       boolean automatAn = true;
      /*Im Rahmen von Aufgabe 6.2 habe ich eine Variable des Datentyp booleans deklariert die angibt, ob das Ger�t/Programm in Betrieb ist und dann damit eine while Schleife gemacht
       die solange meine Variable automatAn true ist mein Programm durchgehend ausf�hrt. Durch den Operator "continue" springt die Schleife am Ende jedes loops wieder zur�ck an den
       Anfang und ist demnach auch direkt wieder ausf�hrbar. */
       
       while(automatAn = true) {																					
    	   																											
       bestellungsPreis = fahrkartenBestellungErfassen();															
       eingezahlterGesamtbetrag = fahrkartenBezahlen(bestellungsPreis); 
       fahrscheinAusgabe();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, bestellungsPreis);
       continue;
       }
    
	}
	}